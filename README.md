Simple example of a program in Rust with a gtk4 interface. This is not the canonical
way to do things, and it is far from the only way to do things. It is the way that I
like to structure a program at the beginning so that when it inevitably grows much
more complex it will remain maintainable.

### Differences from the official examples
* No `meson`, only `cargo`
* Doesn't use Gnome schemas to store data and state, uses native Rust
* No subclassing or composite templates
* Well commented
