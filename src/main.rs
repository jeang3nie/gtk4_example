use gtk::gio::SimpleAction;
use gtk::glib::char::Char;
use gtk::glib::{clone, OptionArg, OptionFlags};
use gtk::Application;
use gtk::prelude::*;

use std::rc::Rc;

// Rather than subclassing and composite templates, I prefer to store a reference
// to each widget that I'm going to be interacting with in a struct. Other data
// which is important to program state can go here as well.
struct Gui {
    window: gtk::ApplicationWindow,
}

impl Gui {
    // Just a simple constructor function
    fn init() -> Gui {
        // We could just as well initialize each widget and pack them into
        // containers with Rust code, but the XML ui format is well suited to
        // creating and organizing a layout
        let builder = gtk::Builder::from_string(include_str!("iface.ui"));
        Gui {
            window: builder.object("mainWindow").unwrap(),
        }
    }

    // This code could go into the `build_ui` function below, but as soon as the
    // application begins to get more complicated it is easy for such a function
    // to grow out of control. I like to split logical blocks out into separate
    // functions like so
    fn add_actions(&self, app: &gtk::Application) -> Actions {
        // Initialize the `Actions` struct, see below
        let actions = Actions::init();

        // Attaching keyboard shortcuts to actions
        app.set_accels_for_action("win.open", &["<primary>O"]);
        app.set_accels_for_action("win.save", &["<primary>S"]);
        app.set_accels_for_action("win.quit", &["<primary>Q"]);

        // Actions must be added to the window for which they are intended
        self.window.add_action(&actions.open);
        self.window.add_action(&actions.save);
        self.window.add_action(&actions.quit);
        actions
    }
}

struct Actions {
    open: SimpleAction,
    save: SimpleAction,
    quit: SimpleAction,
}

impl Actions {
    // As before, these actions could all be initialized in the `build_ui`
    // function, but we want to keep our code organized into smaller, easier to
    // maintain blocks
    fn init() -> Actions {
        Actions {
            open: SimpleAction::new("open", None),
            save: SimpleAction::new("save", None),
            quit: SimpleAction::new("quit", None),
        }
    }

    // Here we use Rust closures to connect our abstract concept of Gtk+ actions
    // to what they are actually going to do. Once again, this does not have to
    // be a separate function, but most programs will be much more coomplex than
    // this small example
    fn connect(&self, gui: &Rc<Gui>) {
        self.open
            .connect_activate(clone!(@strong gui => move |_, _| {
                // gui.dialogs.open.show();
                println!("Not implemented yet");
            }));

        self.save
            .connect_activate(clone!(@strong gui => move |_, _| {
                // gui.dialogs.save.show();
                println!("Not implemented yet");
            }));

        self.quit
            .connect_activate(clone!(@strong gui => move |_, _| {
                gui.window.close();
            }));
    }
}

fn main() {
    let application = gtk::Application::new(Some("org.codeberg.jeang3nie.gtk4_example"), Default::default());
    application.add_main_option(
        "template",
        Char::from(b't'),
        OptionFlags::NONE,
        OptionArg::String,
        "",
        None,
    );
    // Whatever we put into the `build_ui` function is what will run when we
    // call `application.run`
    application.connect_activate(build_ui);
    application.run();
}

fn build_ui(application: &Application) {
    // For a very simple program this is overkill and we can just allow our
    // closures to take ownership of the data being sent into them, including
    // our widgets. This quickly creates ownership problems as the interface and
    // program expands, and you will almost certainly want to use either
    // `std::rc::Rc` or `std::sync::Arc`. Here we call our constructor for the
    // `Gui` struct and put it into an `Rc` reference counted box
    let gui = Rc::new(Gui::init());
    // Adding our actions and connecting them to their closures. Because our
    // `Gui::add_actions()` method above returns the `Actions` struct rather than
    // returning `()`, we can do this in a single line.
    gui.add_actions(application).connect(&gui);
    gui.window.set_application(Some(application));
    // The window must be shown. Here is where the event loop actually begins.
    gui.window.show();
}
